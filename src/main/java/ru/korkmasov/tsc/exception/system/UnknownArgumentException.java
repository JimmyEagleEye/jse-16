package ru.korkmasov.tsc.exception.system;

public class UnknownArgumentException extends Exception {

    public UnknownArgumentException() {
        super("Error! Unknown argument...");
    }

}
