package ru.korkmasov.tsc.exception.empty;

public class EmptyNameException extends Exception {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
