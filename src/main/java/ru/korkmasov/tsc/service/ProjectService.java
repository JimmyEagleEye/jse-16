package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.IProjectRepository;
import ru.korkmasov.tsc.api.IProjectService;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;

import java.util.List;
import java.util.Comparator;
import java.util.Date;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findById(final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException() ;
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final Integer index) throws EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project add(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
        return project;
    }

    @Override
    public void remove(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) throws EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws EmptyIdException, EmptyNameException, ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project=projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) throws EmptyIndexException, EmptyNameException, ProjectNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project=projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(String id) throws EmptyIdException, ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(String name) throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(String id) throws ProjectNotFoundException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(String name) throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

}
