package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;

import java.util.List;
import java.util.Comparator;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findById(String id) throws EmptyIdException;

    Task findByName(String name) throws EmptyNameException;

    Task findByIndex(Integer index) throws EmptyIndexException;

    Task add(String name, String description) throws EmptyNameException;

    Task add(Task task) throws TaskNotFoundException;

    void remove(Task task) throws TaskNotFoundException;

    Task removeById(String id) throws EmptyIdException;

    Task removeByName(String Name) throws EmptyNameException;

    Task removeByIndex(Integer index) throws EmptyIndexException;

    void clear();

    Task updateById(final String id, final String name, final String description) throws EmptyIdException, TaskNotFoundException, EmptyNameException;

    Task updateByIndex(final Integer index, final String name, final String description) throws EmptyIndexException, TaskNotFoundException, EmptyNameException;

    Task startById(String id) throws EmptyIdException, TaskNotFoundException;

    Task startByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task startByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task finishById(String id) throws EmptyIdException, TaskNotFoundException;

    Task finishByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task finishByName(String name) throws TaskNotFoundException, EmptyNameException;

}
