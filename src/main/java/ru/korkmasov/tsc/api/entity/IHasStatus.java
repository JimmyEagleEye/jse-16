package ru.korkmasov.tsc.api.entity;

import ru.korkmasov.tsc.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
