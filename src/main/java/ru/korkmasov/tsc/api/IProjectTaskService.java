package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;


import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(final String projectId) throws ProjectNotFoundException;

    Task bindTaskById(final String taskId, final String projectId) throws ProjectNotFoundException, TaskNotFoundException;

    Task unbindTaskById(final String taskId) throws TaskNotFoundException;

    void removeTaskFromProjectById(String id);

    Project removeProjectById(final String projectId) throws ProjectNotFoundException;

    Project removeProjectByIndex(final Integer index);

    Project removeProjectByName(final String name);

}
