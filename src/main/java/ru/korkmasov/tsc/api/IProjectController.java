package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;


public interface IProjectController {

    void showList();

    void showById() throws ProjectNotFoundException, EmptyIdException;

    void showByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void showByName() throws ProjectNotFoundException, EmptyNameException;

    void create() throws ProjectNotFoundException, EmptyNameException;

    Project add(String name, String description);

    void clear();

    void removeById() throws ProjectNotFoundException, EmptyIdException;

    void removeByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException, EmptyNameException;

    void removeByName() throws ProjectNotFoundException, EmptyNameException;

    void updateByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException, EmptyNameException;

    void updateById() throws ProjectNotFoundException, EmptyIdException, EmptyNameException;

    void startById() throws ProjectNotFoundException, EmptyIdException;

    void startByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void startByName() throws ProjectNotFoundException, EmptyNameException;

    void finishById() throws ProjectNotFoundException, EmptyIdException;

    void finishByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException;

    void finishByName() throws ProjectNotFoundException, EmptyNameException;

}
