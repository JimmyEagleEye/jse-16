package ru.korkmasov.tsc.api;

public interface ICommandController {

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void showCommands();

    void showArguments();

    void displayWelcome();

    void displayWait();

    void showSystemInfo();

    void exit();

}
