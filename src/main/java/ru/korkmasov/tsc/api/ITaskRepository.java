package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Task;

import java.util.List;
import java.util.Comparator;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllTaskByProjectId(String id);

    void removeAllTaskByProjectId(String id);

    Task bindTaskToProjectById(String taskId, String projectId);

    Task unbindTaskById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    void add(Task task);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    void clear();

}
