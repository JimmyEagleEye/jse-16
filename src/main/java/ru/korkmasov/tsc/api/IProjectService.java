package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;


import java.util.List;
import java.util.Comparator;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description) throws EmptyNameException;

    Project findById(String id) throws EmptyIdException;

    Project findByName(String name) throws EmptyNameException;

    Project findByIndex(Integer index) throws EmptyIndexException;

    Project add(Project project) throws ProjectNotFoundException;

    void remove(Project project) throws ProjectNotFoundException;

    Project removeById(String id) throws EmptyIdException;

    Project removeByName(String Name) throws EmptyNameException;

    Project removeByIndex(Integer index) throws EmptyIndexException;

    void clear();

    Project updateById(final String id, final String name, final String description) throws EmptyNameException, EmptyIdException, ProjectNotFoundException;

    Project updateByIndex(final Integer index, final String name, final String description) throws EmptyNameException, EmptyIndexException, ProjectNotFoundException;

    Project startById(String id) throws EmptyIdException, ProjectNotFoundException;

    Project startByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project startByName(String name) throws EmptyNameException, ProjectNotFoundException;

    Project finishById(String id) throws EmptyIdException, ProjectNotFoundException;

    Project finishByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project finishByName(String name) throws EmptyNameException, ProjectNotFoundException;

}
