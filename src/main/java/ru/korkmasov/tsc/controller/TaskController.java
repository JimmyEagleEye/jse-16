package ru.korkmasov.tsc.controller;

import ru.korkmasov.tsc.api.ITaskController;
import ru.korkmasov.tsc.api.ITaskService;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.enumerated.Sort;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

import java.util.List;
import java.util.Arrays;


public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(final ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (Task project : tasks) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showById() throws EmptyIdException, TaskNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByIndex() throws EmptyIndexException, TaskNotFoundException, IndexIncorrectException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByName() throws EmptyIdException, TaskNotFoundException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findById(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    private void showTask(Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project Id: " + task.getProjectId());
    }

    @Override
    public void create() throws TaskNotFoundException, EmptyNameException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return new Task(name, description);
    }

    @Override
    public void removeById() throws EmptyIdException, TaskNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeByIndex() throws EmptyIndexException, TaskNotFoundException, IndexIncorrectException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeByName() throws EmptyNameException, TaskNotFoundException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateByIndex() throws EmptyIndexException, EmptyNameException, TaskNotFoundException, IndexIncorrectException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateByIndex(index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateById() throws EmptyIdException, EmptyNameException, TaskNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void findAllTaskByProjectId() throws ProjectNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findTaskByProjectId(id);
        for (Task task : tasks) {
            System.out.println(task.toString());
        }
    }

    @Override
    public void bindTaskToProjectById() throws TaskNotFoundException, ProjectNotFoundException, EmptyIdException {
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Task taskUpdated = projectTaskService.bindTaskById(taskId, projectId);
        if (taskUpdated == null) System.out.println("Incorrect values");
    }

    @Override
    public void unbindTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task taskUpdated = projectTaskService.unbindTaskById(taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void startById() throws EmptyIdException, TaskNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByIndex() throws EmptyIndexException, TaskNotFoundException, IndexIncorrectException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByName() throws EmptyNameException, TaskNotFoundException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishById() throws EmptyIdException, TaskNotFoundException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByIndex() throws EmptyIndexException, TaskNotFoundException, IndexIncorrectException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByName() throws EmptyNameException, TaskNotFoundException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
