package ru.korkmasov.tsc.controller;

import ru.korkmasov.tsc.api.IProjectController;
import ru.korkmasov.tsc.api.IProjectService;
import ru.korkmasov.tsc.api.IProjectTaskService;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void showById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException{
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    private void showProject(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void create() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public Project add(String name, String description) {
        return null;
    }

    @Override
    public void removeById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project==null) System.out.println("Incorrect values");
        projectTaskService.removeTaskFromProjectById(id);
    }

    @Override
    public void removeByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        projectTaskService.removeTaskFromProjectById(project.getId());
    }

    @Override
    public void removeByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) throw new ProjectNotFoundException();
        projectTaskService.removeTaskFromProjectById(project.getId());
    }

    @Override
    public void updateByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException, EmptyNameException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber()-1;
        final Project project = projectService.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void updateById() throws ProjectNotFoundException, EmptyIdException, EmptyNameException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishByIndex() throws IndexIncorrectException, ProjectNotFoundException, EmptyIndexException {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void finishByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
