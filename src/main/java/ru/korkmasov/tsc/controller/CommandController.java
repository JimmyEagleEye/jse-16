package ru.korkmasov.tsc.controller;

import ru.korkmasov.tsc.api.ICommandService;
import ru.korkmasov.tsc.api.ICommandController;
import ru.korkmasov.tsc.model.Command;
import ru.korkmasov.tsc.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void displayVersion() {
        System.out.println("1.0.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@tsc.com");
    }

    @Override
    public void showCommands(){
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name != null) System.out.println(name);
        }
    }

    @Override
    public void showArguments(){
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) {
            final String arg = command.getArg();
            if (arg != null) System.out.println(arg);
        }
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
